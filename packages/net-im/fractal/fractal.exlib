# Copyright 2018 Rasmus Thomsen <cogitri@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

# FIXME: Using gitlab.exlib is a pain
SCM_REPOSITORY="https://gitlab.gnome.org/World/fractal.git"
SCM_TAG=${PV}

require scm-git cargo gsettings meson gtk-icon-cache

export_exlib_phases src_unpack src_prepare pkg_postrm pkg_postinst

SUMMARY="Matrix group messaging app"

LICENCES="GPL-3"
SLOT="0"

MYOPTIONS="
    providers: ( libressl openssl ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build+run:
        app-spell/enchant:2
        dev-libs/glib:2
        gnome-desktop/gspell[>=1.8]
        gnome-desktop/gtksourceview:3.0
        media-libs/gstreamer:1.0
        media-plugins/gst-plugins-bad:1.0
        media-plugins/gst-plugins-base:1.0
        sys-apps/dbus
        x11-libs/cairo
        x11-libs/gdk-pixbuf:2.0
        x11-libs/gtk+:3
        x11-libs/pango
        providers:libressl? ( dev-libs/libressl:= )
        providers:openssl? ( dev-libs/openssl )
"

fractal_src_unpack() {
    meson_src_unpack
    edo pushd "${MESON_SOURCE}"
    ecargo_fetch
    edo popd
}

fractal_src_prepare() {
    meson_src_prepare

    # We already export CARGO_HOME. If the script exports
    # it again (with a different location) cargo would
    # attempt to download the crates in src_compile again
    edo sed "/CARGO_HOME/d" -i scripts/cargo.sh
}

fractal_pkg_postrm() {
    gsettings_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

fractal_pkg_postinst() {
    gsettings_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

