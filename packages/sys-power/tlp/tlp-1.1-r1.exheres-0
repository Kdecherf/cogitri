# Copyright 2018 Rasmus Thomsen <Rasmus.thomsen@protonmail.com>
# Distributed under the terms of the GNU General Public License v2

require github [ user=linrunner project=TLP tag=${PV} ]

HOMEPAGE="http://linrunner.de/en/tlp/tlp.html"
SUMMARY="Advanced Power Management for Linux"

LICENCES="
    GPL-2
    GPL-3 [[ note = [ tpacpi-bat ] ]]
"

SLOT="0"
PLATFORMS="~amd64"

MYOPTIONS="rdw [[ description = [ Install Radio Device Wizard ] ]]
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]] 
"

# apci-call
DEPENDENCIES="
    build+run:
        net-wireless/iw
        sys-apps/hdparm
        sys-apps/pciutils
        sys-apps/usbutils
        sys-apps/util-linux
        !sys-power/laptop-mode-tools [[ note = [ file conflicts ] ]]
        providers:eudev? ( sys-power/pm-utils )
        providers:systemd? ( !sys-power/pm-utils )
        rdw? ( net-apps/NetworkManager )
    suggestion:
        sys-apps/ethtool [[ description = [ Used to disable wake on lan ] ]]
        sys-apps/smartmontools [[ description = [ Used to show hard disk drive SMART data in tlp-stat ] ]]
"

DEFAULT_SRC_INSTALL_PARAMS=(
    TLP_BIN="/usr/$(exhost --target)/bin"
    TLP_SBIN="/usr/$(exhost --target)/bin"
    TLP_PLIB="/usr/$(exhost --target)/lib/pm-utils"
    TLP_ULIB="/usr/$(exhost --target)/lib/udev"
    TLP_SYSD="/usr/$(exhost --target)/lib/systemd/system"
)

src_prepare() {
    # remove ubuntu specific stuff
    edo sed -i "/\/lsb\//d" tlp.init
}

src_install() {
    emake -j1 install-tlp install-man \
        DESTDIR="${IMAGE}" \
        "${DEFAULT_SRC_INSTALL_PARAMS[@]}" \
        $(option providers:systemd TLP_NO_INIT='1') \
        $(option providers:systemd TLP_NO_PMUTILS='1') \
        $(option providers:systemd TLP_WITH_SYSTEMD='1') \
        $(option rdw install-rdw)
}

