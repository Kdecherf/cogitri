# Copyright 2007-2008 Alexander Færøy <eroyf@exherbo.org>
# Copyright 2008 Bernd Steinhauser <berniyh@exherbo.org>
# Copyright 2008 Ingmar Vanhassel <ingmar@exherbo.org>
# Copyright 2009 Xavier Barrachina <xabarci@ega.upv.es>
# Copyright 2010 Cecil Curry <leycec@gmail.com>
# Copyright 2012 Wouter van Kesteren <woutershep@gmail.com>
# Copyright 2017 Rasmus Thomsen <Rasmus.thomsen@protonmail.com>
# Distributed under the terms of the GNU General Public License v2

require mozilla-nightly [ co_project=browser ]

SCM_REPOSITORY="https://hg.mozilla.org/mozilla-central"

require scm-hg

PLATFORMS="~amd64 ~x86"

SUMMARY="Mozilla's standalone web browser ( Nightly version )"
HOMEPAGE="http://www.mozilla.com/en-US/${PN}"

LICENCES="
    || ( MPL-1.1 MPL-2.0 GPL-2 GPL-3 LGPL-2.1 LGPL-3 )
"

# Do not use system sqlite since it requires secure_delete to be enabled by default.
# This incurs a preformance penalty for zeroing data on delete.
# There is a patch to make xulrunner do a PRAGMA secure_delete = ON;
# at runtime to avoid the overhead on other programs. But upstream rejected it.
# see: https://bugzilla.mozilla.org/show_bug.cgi?id=546162 and
# https://bugzil.la/1049920
#
# If we decide to accept this penalty or decide
# to use the unsupported patch then we will also need:
#
# threadsafe: already enabled in the exheres
# unlock-notify: already an option in the exheres
# fts3: seems to be enabled by default (-DSQLITE_ENABLE_FTS3 in Makefile.am)
#       documentation however claims that it is disabled by default...

#--with-system-nss ( fails to build )
#--with-system-nspr ( fails to link )
#--enable-system-pixman ( fails regularly)
#--enable-system-cairo ( fails regularly)
#--with-system-icu ( currently causes firefox to not start )

MOZILLA_SRC_CONFIGURE_PARAMS+=(
    --disable-necko-wifi
    --disable-shared-js # https://bugzil.la/1112945
    --disable-synth-speechd # seems dependencyless but dlopens libspeechd.so.2
    --disable-warnings-as-errors
    --enable-chrome-format=omni
)

MOZILLA_SRC_CONFIGURE_OPTIONS=(
    'libc:musl --disable-jemalloc --enable-jemalloc'
)

src_install() {
    mozilla-nightly_src_install

    insinto /usr/share/applications
    doins "${FILES}"/${PN}.desktop

    # Don't collide with firefox
    edo rm "${IMAGE}"/usr/$(exhost --target)/bin/firefox
    dosym /usr/$(exhost --target)/lib/firefox-nightly/firefox /usr/$(exhost --target)/bin/firefox-nightly

    # some resources (ex: jsloader, jssubloader) are put into ommi-jar so the directory is empty
    edo find "${IMAGE}" -type d -empty -delete

    edo hereenvd 50firefox-nightly <<EOF
COLON_SEPARATED="MOZ_PLUGIN_PATH"
MOZ_PLUGIN_PATH="/usr/$(exhost --target)/lib/nsbrowser/plugins:/opt/nsbrowser/plugins"
EOF

    # allow installation of distributed extensions and read/use system locale on runtime
    insinto /usr/$(exhost --target)/lib/${PN}/browser/defaults/preferences
    hereins all-exherbo.js <<EOF
pref("extensions.autoDisableScopes", 3);
pref("intl.locale.matchOS", true);
EOF
}

